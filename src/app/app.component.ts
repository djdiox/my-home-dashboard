import { Component } from '@angular/core';
import { ModalService, ModalOptions } from './services/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public currentModalOptions: ModalOptions;

  constructor(private modalService: ModalService) {
    this.modalService.CurrentModalOptions
      .subscribe(options => this.currentModalOptions = options);
  }
}
