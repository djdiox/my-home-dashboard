import { Http, Response } from '@angular/http';
import { logWarnings } from 'protractor/built/driverProviders';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { User } from './users.service';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  private _isLoggedIn = false;

  private _currentUser: Subject<User>;

  private loginUrl = `${environment.baseUrl}/login`;

  constructor(private http: Http) {
    this._currentUser = new Subject<User>();
  }
/**
 * Logs in the User and sets it in the Subject
 *
 * @param {string} email the email of the user
 * @param {string} password the password of the user
 * @returns {Observable} HttpObservable
 * @memberof AuthService
 */
public loginUser(email: string, password: string) {
    return this.http.post(this.loginUrl, { email, password })
      .subscribe((res: Response) => {
        if (!res.ok) { return; }
        const user = res.json();
        this._currentUser.next(user);
      });
  }
}
