import { Injectable } from '@angular/core';

export interface User {
  name: string;
  password: string;
  services: Array<string>;
  roles: Array<string>;
}
@Injectable()
export class UsersService {

  constructor() { }

}
