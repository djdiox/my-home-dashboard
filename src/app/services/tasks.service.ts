import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Headers, Http, Response } from '@angular/http';
export interface Task {
  name: string;
  startDate: Date;
}

@Injectable()
export class TasksService {
  private _tasksUrl = `${environment.baseUrl}/tasks`;
  private _tasks: Subject<Task[]>;
  constructor(private http: Http) {
    this._tasks = new Subject<Task[]>();
  }

/**
 * Gets the Tasks list as an Observable
 *
 * @readonly
 * @memberof TasksService
 */
public get tasks() {
    return this._tasks.asObservable();
  }

/**
 * Fetches the tasks on the server
 *
 * @returns {Observable} the http Observable of the Request
 * @memberof TasksService
 */
public fetchtTasks() {
    return this.http
      .get(this._tasksUrl)
      .do((response) => {
        const tasks = response.json();
        this._tasks.next(tasks);
      })
      .catch((err) => {
        return [];
      })
      .subscribe();
  }
/**
 * Saves a Task in the database
 *
 * @param {Task} task the task to be saved
 * @returns {Observable} the Observable of the http request.
 * @memberof TasksService
 */
public saveTask(task: Task) {
    return this.http
      .post(this._tasksUrl, task)
      .do((response: Response) => {
        const newTask = response.json();
        this._tasks.next(newTask);
      });
  }
}
