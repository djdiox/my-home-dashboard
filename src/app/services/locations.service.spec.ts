import { TestBed, inject } from '@angular/core/testing';

import { LocationsService } from './locations.service';
import { HttpModule } from '@angular/http';
import { NotificationService } from './notification.service';

describe('LocationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LocationsService,
        NotificationService
      ],
      imports: [
        HttpModule,
      ],
    });
  });

  it('should be created', inject([LocationsService], (service: LocationsService) => {
    expect(service).toBeTruthy();
  }));
});
