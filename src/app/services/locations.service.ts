import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs/Subject';
import { NotificationService } from './notification.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LocationsService {

  private _locationsUrl = `${environment.baseUrl}/locations`;
  private _locations: Subject<Location[]>;

  constructor(private http: Http, private notificationsService: NotificationService) {
    this._locations = new Subject<Location[]>();
  }

  public get Locations(): Observable<Location[]> {
    return this._locations.asObservable();
  }
  public fetchLocations() {
    return this.http
      .get(this._locationsUrl)
      .subscribe(response => {
        const currentLocations = response.json();
        this._locations.next(currentLocations);
      });
  }

  public saveLocation(location: Location) {
    return this.http
      .post(this._locationsUrl, location)
      .subscribe(response => {
        const newLocation = response.json();
        this._locations.next(newLocation);
      });
  }
}

interface Location {
  uuid: String;
  latitude: Number;
  longitude: Number;
  name: String;
  description: String;
}
