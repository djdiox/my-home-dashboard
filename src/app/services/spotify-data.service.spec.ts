import { TestBed, inject } from '@angular/core/testing';

import { SpotifyDataService } from './spotify-data.service';
import { HttpModule } from '@angular/http';

describe('SpotifyDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpotifyDataService],
      imports: [
        HttpModule
      ]
    });
  });

  it('should be created', inject([SpotifyDataService], (service: SpotifyDataService) => {
    expect(service).toBeTruthy();
  }));
});
