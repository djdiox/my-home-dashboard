import { TestBed, inject } from '@angular/core/testing';

import { TodosService } from './todos.service';
import { HttpModule } from '@angular/http';

describe('TodosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodosService],
      imports: [
        HttpModule
      ]
    });
  });

  it('should be created', inject([TodosService], (service: TodosService) => {
    expect(service).toBeTruthy();
  }));
});
