import { Observable, Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

export interface ModalOptions {
  name: string;
  id: string;
  visible: boolean;
  type?: string;
  header?: string;
  body?: string;
}
@Injectable()
export class ModalService {

  private _modalOptions: Subject<ModalOptions>;
  public currentModalOptions: ModalOptions;

  constructor() {
    this._modalOptions = new Subject<ModalOptions>();
    this._modalOptions
      .subscribe((modalOptions: ModalOptions) => {
        this.currentModalOptions = modalOptions;
      });
  }

  public get CurrentModalOptions(): Observable<ModalOptions> {
    return this._modalOptions.asObservable();
  }

  openModal(options: ModalOptions) {
    this._modalOptions.next(options);
  }

  closeModal() {
    this.currentModalOptions.visible = false;
    this._modalOptions.next(this.currentModalOptions);
  }
}
