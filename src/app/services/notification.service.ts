import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
export interface Message {
  title: string;
  body: string;
  type: string;
  id: Number;
  displayTime?: 250;
}
@Injectable()
export class NotificationService {

  private _messages: Subject<Message>;

  private currentId = 0;

  constructor() {
    this._messages = new Subject<Message>();
  }

  public get Messages(): Observable<Message> {
    return this._messages.asObservable();
  }

  public pushNotifcation(title: string, type: string, body: string, delayTime?: Number) {
    this.currentId++;
    this._messages.next(<Message>{ id: this.currentId, title, body, type, delayTime });
  }
}
