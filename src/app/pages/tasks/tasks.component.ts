import { Component, OnInit } from '@angular/core';
import { Task } from '../../services/tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  currentTask: Task = {
    name: '',
    startDate: new Date()
  };

  constructor() { }

  ngOnInit() {
  }

}
