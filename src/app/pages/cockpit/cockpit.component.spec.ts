import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CockpitComponent } from './cockpit.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from '../../pipes/filter.pipe';
import { TodosService } from '../../services/todos.service';
import { Http, HttpModule } from '@angular/http';

describe('CockpitComponent', () => {
  let component: CockpitComponent;
  let fixture: ComponentFixture<CockpitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CockpitComponent,
        FilterPipe
      ],
      imports: [
        FormsModule,
        HttpModule
      ],
      providers: [
        TodosService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
