import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MouseEvent, MapsAPILoader } from '@agm/core';
import { LocationsService } from '../../services/locations.service';
import { } from 'googlemaps';
import { FormControl } from '@angular/forms';
import { ModalService, ModalOptions } from '../../services/modal.service';


@Component({
  selector: 'app-world-map',
  templateUrl: './world-map.component.html',
  styleUrls: ['./world-map.component.scss']
})
export class WorldMapComponent implements OnInit {

  // currentLabel: string;
  currentMarker: Marker;
  // google maps zoom level
  zoom = 8;
  // initial center position for the map
  lat = 51.673858;
  lng = 7.815982;
  public searchControl: FormControl;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  markers: Marker[] = [
    {
      id: 0,
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      infoWindow: 'Standort A',
      draggable: true
    },
    {
      id: 1,
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      infoWindow: 'Standort B',
      draggable: false
    },
    {
      id: 2,
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      infoWindow: 'Standort C',
      draggable: true
    }
  ];
  userSettings = {};
  currentLocations: Location[];
  currentModalOptions: ModalOptions;

  constructor(
    private locationsService: LocationsService,
    private modalService: ModalService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    this.locationsService
      .Locations
      .subscribe((locations: any) => {
        this.markers = locations.map(location => <Marker>location);
      });
    this.modalService
      .CurrentModalOptions
      .subscribe(modalOptions =>
        this.currentModalOptions = modalOptions);
    this.locationsService.fetchLocations();
  }


  saveLocation() {

  }

  /**
   * Event listener, when the marker on the map has been clicked
   *
   * @param {string} label the label of the current marker
   * @param {number} index the index of the current marker
   * @memberof WorldMapComponent
   */
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
    // this.markers.splice(index, 1);
    this.currentMarker = this.markers[index];
  }

  onSubmit() {
    // this.markers[this.currentMarker.id].label = this.currentLabel;
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      id: this.markers.length,
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      label: (this.markers.length + 1).toString(),
      infoWindow: '',
      draggable: true
    });
  }

  autoCompleteCallback1($event: any) {
    console.log($event);
  }
  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }


  ngOnInit() {
    this.currentMarker = {
      id: 0,
      draggable: true,
      infoWindow: '',
      label: '',
      lat: 0,
      lng: 0
    };

    this.searchControl = new FormControl();

    // Google Places Tutorial http://brianflove.com/2016/10/18/angular-2-google-maps-places-autocomplete/
    // set current position
    this.setCurrentPosition();

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }
}

// just an interface for type safety.
interface Marker {
  id: number;
  lat: number;
  lng: number;
  label?: string;
  infoWindow?: string;
  draggable: boolean;
}
