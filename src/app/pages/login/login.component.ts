import { Component, OnInit } from '@angular/core';
import { ModalOptions, ModalService } from '../../services/modal.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  constructor(private modalService: ModalService, private authService: AuthService) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalService.closeModal();
  }

  loginUser(mail: string, password: string) {
    this.authService.loginUser(mail, password);
  }
}
