import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundExplorerComponent } from './sound-explorer.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { SpotifyDataService } from '../../services/spotify-data.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

describe('SoundExplorerComponent', () => {
  let component: SoundExplorerComponent;
  let fixture: ComponentFixture<SoundExplorerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SoundExplorerComponent],
      imports: [
        HttpClientModule,
        HttpModule,
        InlineSVGModule,
      ],
      providers: [
        SpotifyDataService,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
