import { Component, Input, OnInit, ViewEncapsulation, HostListener, ElementRef, ViewChild } from '@angular/core';
import { ModalService, ModalOptions } from '../../services/modal.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() modalOptions = {
    body: '',
    id: '',
    header: '',
    name: '',
    type: '',
    visible: false
  };

  offClickHandler(event: any) {
    if (this._eref.nativeElement.children &&
      this._eref.nativeElement.children.length > 0 &&
      !this._eref.nativeElement.children[0].contains(event.target) &&
      this.modalOptions.visible === true) { // check click origin
      this.modalService.closeModal();
    }
  }

  constructor(private modalService: ModalService, private _eref: ElementRef) {
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.offClickHandler.bind(this));
  }
  ngOnInit() {
    setTimeout(() => {
      if (this.modalOptions.visible) {
        document.addEventListener('click', this.offClickHandler.bind(this)); // bind on doc
      }
    }, 100);
  }

}
