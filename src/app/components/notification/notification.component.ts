import { Component, OnInit } from '@angular/core';
import { NotificationService, Message } from '../../services/notification.service';
import { setTimeout } from 'timers';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  public currentNotifications: Message[];

  constructor(private notificationService: NotificationService) {
    this.currentNotifications = [];
    notificationService.Messages
      .subscribe((current) => {
        this.currentNotifications.push(current);
        setTimeout(() => this.dismissNotification(current.id), current.displayTime);
      });
  }

  ngOnInit() {
  }

  public dismissNotification(id: Number) {
    this.currentNotifications.filter((notification) => notification.id !== id);
  }
}
